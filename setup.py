import setuptools

setuptools.setup(
    name="salut", # Replace with your own username
    version="1.0.0",
    author="Martin Naughton",
    author_email="martin@martin.com",
    description="Software for showing how to build a package",
    long_description="Long des",
    long_description_content_type="text/markdown",
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.7",
)
